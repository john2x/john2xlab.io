==================================================================
https://keybase.io/john2x
--------------------------------------------------------------------

I hereby claim:

 * I am an admin of https://www.john2x.com
 * I am john2x (https://keybase.io/john2x) on keybase.
 * I have a public key ASCY-Xaf7jXGFun16lwCT2cOacnua4JMadfA1rtR4r_tZgo

To do so, I am signing this object:

{
 "body": {
   "key": {
     "eldest_kid": "012098f9769fee35c616e9f5ea5c024f670e69c9ee6b824c69d7c0d6bb51e2bfed660a",
     "host": "keybase.io",
     "kid": "012098f9769fee35c616e9f5ea5c024f670e69c9ee6b824c69d7c0d6bb51e2bfed660a",
     "uid": "fb1f94a70bcff3bbf9cf4367ea060119",
     "username": "john2x"
   },
   "merkle_root": {
     "ctime": 1541144363,
     "hash": "5fa962bd01f4b85aaadfb88c61d6b55be06a531337faaa793b890447c64d40b1ec4c59fcf67e88e1f82b3148299ecef4a9e3d0b03575a72dd309b6eb56312b16",
     "hash_meta": "79fc40a38fbe3cde675cc9bad420fb8ba6f1bc5c86475587e039152f644e7231",
     "seqno": 3885601
   },
   "service": {
     "entropy": "rlZn0cWd1JBfSOgkliQvTaJD",
     "hostname": "www.john2x.com",
     "protocol": "https:"
   },
   "type": "web_service_binding",
   "version": 2
 },
 "client": {
   "name": "keybase.io go client",
   "version": "2.8.0"
 },
 "ctime": 1541144413,
 "expire_in": 504576000,
 "prev": "431141ff53d8502b6e93c2a5a0bec4eb81a23bd197ad67ef0e2ba005eb3440ca",
 "seqno": 4,
 "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgmPl2n+41xhbp9epcAk9nDmnJ7muCTGnXwNa7UeK/7WYKp3BheWxvYWTESpcCBMQgQxFB/1PYUCtuk8KloL7E64GiO9GXrWfvDiugBes0QMrEIJt9zGZ+2GY3xckINQVrJG/zTx2CL2uGd9u6HqhgJ3MwAgHCo3NpZ8RApRlnW/3/F0RMOWKVhDfA1UzF+7IlSQJhpxaS3l2jU5/K+1Xz9weCgDDMMO/G9Xj+8aUkOffHArYYSqmw3OE/BqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIFaQ/+tN7oH0wjywOgrGdspCfo2kG/fFRbKm6buzR9Lco3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/john2x
